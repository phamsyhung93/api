/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.TblRole;
import entities.TblUser;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author hung
 */
public class RoleImpl implements IRole{

    @Override
    public List<TblRole> getAllRole() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            List list = session.createQuery("from TblRole ").list();
            session.getTransaction().commit();
            session.close();
            return list;
        } catch (Exception e) {
            session.getTransaction().rollback();
            session.close();
            return null;
        }
    }

    @Override
    public TblRole getRoleById(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            TblRole role = (TblRole) session.createQuery("from TblRole where id = :id")
                    .setParameter("id", id)
                    .uniqueResult();
            session.getTransaction().commit();
            session.close();
            return role;
        } catch (Exception e) {
            session.getTransaction().rollback();
            session.close();
            return null;
        }
    }
    
}
