/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Model.UserModel;
import entities.TblRole;
import entities.TblUser;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author hung
 */
public class UserDaoImpl implements IUserDao{
    
    
    @Override
    public TblUser getUser(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            TblUser user = (TblUser) session.createQuery("from TblUser where username = :username and password =:password and status = true ")
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .uniqueResult();
            session.getTransaction().commit();
            session.close();
            return user;
        } catch (Exception e) {
            session.getTransaction().rollback();
            session.close();
            return null;
        }
    }
    
    
     @Override
    public List<TblUser> getAllUser() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            List list = session.createQuery("from TblUser where status = true").list();
            session.getTransaction().commit();
            session.close();
            return list;
        } catch (Exception e) {
            session.getTransaction().rollback();
            session.close();
            return null;
        }
        
    }

    @Override
    public TblUser getUserByUserId(int UserId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            TblUser user = (TblUser) session.createQuery("from TblUser where status = true and id = :id")
                    .setParameter("id", UserId)
                    .uniqueResult();
            session.getTransaction().commit();
            session.close();
            return user;
        } catch (Exception e) {
            session.getTransaction().rollback();
            session.close();
            return null;
        }
    }
    
    @Override
    public boolean addUser(UserModel u) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            TblUser us = new TblUser();
            RoleImpl r = new RoleImpl();
            TblRole role = r.getRoleById(u.getRoleId());
            us.setId(u.getId());
            us.setRoleId(role);
            us.setBirthday(u.getBirthday());
            us.setCreateAt(u.getCreateAt());
            us.setCreateBy(u.getCreateBy());
            us.setDisplayName(u.getDisplayName());
            us.setDisplayName(u.getDisplayName());
            us.setEmail(u.getEmail());
            us.setFirstName(u.getFirstName());
            us.setLastName(u.getLastName());
            us.setPassword(u.getPassword());
            us.setPhone(u.getPhone());
            us.setSex(u.getSex());
            us.setStatus(u.getStatus());
            us.setUpdateAt(u.getUpdateAt());
            us.setUpdateBy(u.getUpdateBy());
            us.setUsername(u.getUsername());
            session.beginTransaction();
            session.save(us);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return false;
    }

    @Override
    public boolean updateUser(UserModel u) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            TblUser us = getUserByUserId(u.getId());
            RoleImpl r = new RoleImpl();
            TblRole role = r.getRoleById(u.getRoleId());
            us.setRoleId(role);
            us.setBirthday(u.getBirthday());
            us.setCreateAt(u.getCreateAt());
            us.setCreateBy(u.getCreateBy());
            us.setDisplayName(u.getDisplayName());
            us.setDisplayName(u.getDisplayName());
            us.setEmail(u.getEmail());
            us.setFirstName(u.getFirstName());
            us.setLastName(u.getLastName());
            us.setPassword(u.getPassword());
            us.setPhone(u.getPhone());
            us.setSex(u.getSex());
            us.setStatus(u.getStatus());
            us.setUpdateAt(u.getUpdateAt());
            us.setUpdateBy(u.getUpdateBy());
            us.setUsername(u.getUsername());
            session.beginTransaction();
            session.update(us);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
        }
        return false;
    }

}

